// Homework_16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include <ctime>


int main()
{
    const int N = 9;

    int mas[N][N];
    //zapolnenie massiva
    for (int i = 0; i < N; ++i) 
    {
        for (int j = 0; j < N; ++j)
        {
            mas[i][j] = i + j;
            std::cout << mas[i][j] << " : ";
        }
        std::cout << "\n";
    }
    
  
    // current date/time
    time_t now = time(0);

    int indx = now % N;
    

    int sum = 0;
    for (int i = 0; i < N; ++i)
    {
        sum += mas[indx][i];

    }
    
    std::cout << "Index : " << indx << "\n";
    std::cout << "Sum : " << sum << "\n";


 

    
    
}

